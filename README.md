# Concur Spend Analysis

## Overview

This project will update the Concur Spend Analysis by reorganizing the data files
and moving the analysis to the FBI project folder. It also includes splitting
hisorical data from current data.

Contact for this project is Dustin Nesbit.

### To Do Process

- Add cost center name and crewmember name to Concur line item level data report.
- Remove crewmember data table from existing Spotfire analysis.
- Embed historical line item data and analysis for faster load times.
- Move analysis from payments to fbi folder.

