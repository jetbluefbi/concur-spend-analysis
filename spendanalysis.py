from pathlib import Path
from tkinter import Tk
from tkinter import filedialog

import numpy as np
import pandas as pd


HOME_DIR = Path.home().resolve()
SPOTFIRE_DATA_DIR = Path('//sscdfs_new/sharedfile/fbi/concur-spend-analysis/').resolve()

def monthly_run():
    """Process the *line-item-data* report from Concur so that it can be used
    for the *concur-spend-analysis* Spotfire dashboard.

    This script should be ran **after** downloading the *line-item-data* report
    from Concur.

    How to download the *line-item-data* report from Concur:
    1. Login to Concur @ https://apps.jetblue.com/applications/c/cliqbooksso/.
    2. Go to Reporting > Intelligence.
    3. Go to Public Folders > Jetblue Airways Cooporation (p0008202wyph)
    > fbi > concur-spend-analysis.
    4. Click on "line-item-data" and enter the date range for report.
        a. The start date should be the beginning of the most recent six-month
        period (e.g. if today is 2018-10-02 the start date should be 2018-07-01).
        b. The end date should be the first day of the current month (e.g. if
        today is 2018-10-10 the end date should be 2018-10-01).
    5. Save the report to a location you'll remember.
    """

    root = Tk()
    # TODO (Tyler)
    # Use the `filedialog.askopenfilename()` function to get downloaded
    # line-item-data report location from user.
    # Get help with syntax and options @ https://pythonspot.com/tk-file-dialogs/.
    infile = filedialog.askopenfilename(
        # options go here
        # HINT: use initialdir=HOME_DIR
    )
    
    # double check that report was ran with correct options.
    if infile.suffix != '.xlsx':
        raise TypeError('line-item-data report must be ran as "Excel 2007" option in Concur.')
    root.destroy()
    
    # TODO (Tyler)
    # Use the `pandas.read_excel()` function to load concur report into a dataframe.
    # Remember a report may have more than one sheet of data.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_excel.html.
    dfs = pd.read_excel(
        # options go here
        # HINT: use io=infile
        # HINT: use sheet_name=None
    )

    # TODO (Tyler)
    # Use the `pandas.concat()` function to combine sheet dataframes into a single dataframe.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.concat.html.
    df = pd.concat(
        # options go here
        # HINT: use objs=dfs
        # HINT: use ignore_index=True
    )

    # TODO (Tyler)
    # Use the `Path.joinpath()` function to specify the export filename for the dataframe.
    # HINT: apply the function to the  `SPOTFIRE_DATA_DIR` varibale which is a `Path` object.
    # HINT: a shortcut for the `Path.joinpath()` function is `Path` object / 'paths' / 'to' / 'join'
    outfile = # function goes here
    
    # TODO (Tyler)
    # Use the `pandas.DataFrame.to_csv()` function to export the combined dataframe to a .csv file used by the Spotfire analysis.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_csv.html.
    df.to_csv(
        # options go here
        # HINT: use path_or_buf=outfile
        # HINT: use index=False
    )


def six_month_run():
    """Append the *line-item-data* report from Concur to the *line-item-historical*
    file.

    This script should be ran **after** downloading the *line-item-data* report
    for the previous six-month period from Concur.

    How to download the *line-item-data* report from Concur:
    1. Login to Concur @ https://apps.jetblue.com/applications/c/cliqbooksso/.
    2. Go to Reporting > Intelligence.
    3. Go to Public Folders > Jetblue Airways Cooporation (p0008202wyph).
    > fbi > concur-spend-analysis.
    4. Click on "line-item-data" and enter the date range for report.
        a. The start date should be the beginning of the previous six-month
        period (e.g. if today is 2019-01-02 the start date should be 2018-07-01).
        b. The end date should be the first day of the current six-month period
        (e.g. if today is 2019-01-02 the end date should be 2019-01-01).
    5. Save the report to a location you'll remember.

    After running this script the user will have to open the Spotfire analysis
    file in the desktop application and refresh the *line-item-historical*
    data table.

    How to refresh the *line-item-historical* data table:
    1. Open the TIBCO Spotfire Desktop Client.
    2. Go to File > Open From > Library...
    3. Go to Library > fbi > concur-spend-analysis.
    4. Open "concur-spend-analysis".
    5. Go to Edit > Data Table Properties.
    6. Select "line-item-historical" so that it is highlighted.
    7. Click Refresh Data > Without Prompt.
    8. Click *OK* to close the prompt.
    9. Go to File > Save and click *Yes* when it asks if you would like to
    continue.
    """

    root = Tk()
    # TODO (Tyler)
    # Use the `filedialog.askopenfilename()` function to get downloaded
    # line-item-data report location from user.
    # Get help with syntax and options @ https://pythonspot.com/tk-file-dialogs/.
    infile = filedialog.askopenfilename(
        # options go here
        # HINT: use initialdir=HOME_DIR
    )
    
    # Double check that report was ran with correct options.
    if infile.suffix != '.xlsx':
        raise TypeError('line-item-data report must be ran as "Excel 2007" option in Concur.')
    root.destroy()

    # Create an empty list for dataframes to concatenate.
    dfs = []

    # TODO (Tyler)
    # Use the `pandas.read_excel()` function to load concur report into a dataframe.
    # Remember a report may have more than one sheet of data.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_excel.html.
    dfs_dict = pd.read_excel(
        # options go here
        # HINT: use io=infile
        # HINT: use sheet_name=None
    )

    # TODO (Tyler)
    # Iterate over the **values** of the `dfs_dict` dictionary and append the dataframes to the `dfs` variable.
    # Get help with for loop (iteration) syntax @ https://docs.python.org/3/tutorial/controlflow.html#for-statements.
    # Get help with dictionary syntax and functions (how to get values) @ https://docs.python.org/3/library/stdtypes.html#dictionary-view-objects.
    # Get help with list syntax and functions (how to append values) @ https://docs.python.org/3/tutorial/datastructures.html#more-on-lists.
    # functions go here

    # TODO (Tyler)
    # Use the `Path.joinpath()` function to specify the filename for the *line-item-historical* file.
    # HINT: apply the function to the  `SPOTFIRE_DATA_DIR` varibale which is a `Path` object.
    # HINT: a shortcut for the `Path.joinpath()` function is `Path` object / 'paths' / 'to' / 'join'
    fp = # function goes here

    # TODO (Tyler)
    # Use the `pandas.read_csv()` function to load *line-item-historical* file into a dataframe.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html.
    df = pd.read_csv(
        # options go here
        # HINT: use filepath_or_buffer=infile
    )

    # TODO (Tyler)
    # Append the the dataframe to the `dfs` variable.
    # Get help with list syntax and functions (how to append values) @ https://docs.python.org/3/tutorial/datastructures.html#more-on-lists.
    # function goes here

    # TODO (Tyler)
    # Use the `pandas.concat()` function to combine sheet dataframes into a single dataframe.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.concat.html.
    df = pd.concat(
        # options go here
        # HINT: use objs=dfs (`dfs` should contain at least two dataframe objects)
        # HINT: use ignore_index=True
    )

    # TODO (Tyler)
    # Use the `pandas.DataFrame.to_csv()` function to export the combined dataframe to the *line-item-historical* file.
    # Get help with syntax and options @ https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_csv.html.
    df.to_csv(
        # options go here
        # HINT: use path_or_buf=fp
        # HINT: use index=False
    )


if __name__ == '__main__':
    pass