def set_document_property(document_property, value):
    try:
        print 'Before: %s.' % (Document.Properties[document_property])
        Document.Properties[document_property] = value
        print 'After: %s.' % (Document.Properties[document_property])
    except:
        raise TypeError('`value` of inappropriate type for document property.')

set_document_property(document_property, value)

