def set_visualization_axis(axis, expression, label):
    if not (axis == 'x' or axis == 'y'):
        raise ValueError('`axis` must be either "x" or "y".')
    expression_prop = axis.upper() + 'AxisExpression'
    label_prop = axis.upper() + 'AxisLabel'
    set_document_property(expression_prop, expression)
    set_document_property(label_prop, label)


def set_document_property(document_property, value):
    try:
        print 'Before: %s.' % (Document.Properties[document_property])
        Document.Properties[document_property] = value
        print 'After: %s.' % (Document.Properties[document_property])
    except:
        raise TypeError('`value` of inappropriate type for document property.')


# execute script
set_visualization_axis(axis, expression, label)