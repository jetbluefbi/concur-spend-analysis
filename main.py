from pathlib import Path

import pandas as pd
import xlrd


BASE_DIR = Path(__file__).resolve().parent
DATA_DIR = BASE_DIR.joinpath('data')
OUTPUT_FILE = BASE_DIR.joinpath('line-item-historical.csv')
# or this shortcut for Path.joinpath()
# OUTPUT_FILE = DATA_DIR / 'line-item-historical.csv'

def main():
    dfs = list()
    dtype = {
        'entry_key': str,
        'report_key': str,
        'crewmember_id': str,
        'crewmember_cost_center_code': str,
        'cost_center_code': str,
    }

    for file in DATA_DIR.iterdir():
        print(f'Creating dataframe from {str(file)}.')
        for df in pd.read_excel(file, sheet_name=None, dtype=dtype).values():
            dfs.append(df)
    print('Combining dataframes.')
    df = pd.concat(dfs, ignore_index=True)
    df.sort_values(by=['first_submitted_date'], inplace=True)
    
    df.to_csv(OUTPUT_FILE, index=False)

        
if __name__ == '__main__':
    main()

    