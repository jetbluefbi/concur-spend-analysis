def set_document_properties(document_properties, values):
    """Set values for an arbitrary amount of document_properties.

    Parameters
    ----------
    document_properties : str
        A commma-seperated list of existing document property names.
    values : str
        A comma seperated list of values to set.

    Returns
    -------
    None

    Example
    -------
    >>> document_properties = prop0, prop1, prop2
    >>> values = value0, value1, value2
    >>> set_document_properties(document_properties, values)
    """

    document_properties = document_properties.split(',')
    values = values.split(',')

    for i in range(len(document_properties)):
        # print '%s: %s.' % (document_properties[i], values[i])
        set_document_property(document_properties[i], values[i])


def set_document_property(document_property, value):
    print 'Before: %s.' % (Document.Properties[document_property])
    try:
        Document.Properties[document_property] = value
        print 'After: %s.' % (Document.Properties[document_property])
    except TypeError:
        raise('Provided value does not match type of %s.' % (document_property))


# execute script
set_document_properties(document_properties, values)